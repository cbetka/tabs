class Tabs {
	constructor(tabs, tabsContent, parent) {
		this.tabs = tabs;
		this.tabsContent = tabsContent;
		this.parent = parent;
		this.itemsTabs = [].slice.call(this.parent.querySelectorAll(`.${this.tabs}`));
		this.itemsContents = [].slice.call(this.parent.querySelectorAll(`.${this.tabsContent}`));

		this.initTabs();
	}

	initTabs() {
		this.itemsTabs.forEach((item, i) => {
			item.addEventListener('click', (event) => {
				event.preventDefault();
				if (!item.classList.contains(`${this.tabs}--active`) && !item.classList.contains('disabled')) {
					this.itemsTabs.forEach((tab) => {
						tab.classList.remove(`${this.tabs}--active`);
					});
					this.itemsTabs[i].classList.add(`${this.tabs}--active`);

					this.itemsContents.forEach((content) => {
						content.classList.remove(`${this.tabsContent}--active`);
					});
					this.itemsContents[i].classList.add(`${this.tabsContent}--active`);
				}
			});
		});
	}
}

const parent = document.querySelector('.wrapper');
new Tabs('wrapper__tab', 'wrapper__content-item', parent);