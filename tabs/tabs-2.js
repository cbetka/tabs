var jsTriggers = document.querySelectorAll('.tab');

jsTriggers.forEach(function(trigger) {
   trigger.addEventListener('click', function() {
      var id = this.getAttribute('data-tab'),
          content = document.querySelector('.content-item[id="'+id+'"]'),
          activeTrigger = document.querySelector('.tab.active'),
          activeContent = document.querySelector('.content-item.active');
      
      activeTrigger.classList.remove('active');
      trigger.classList.add('active');
      
      activeContent.classList.remove('active');
      content.classList.add('active');
   });
});